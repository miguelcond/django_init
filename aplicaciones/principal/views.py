# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from .models import Persona
from .forms import PersonaForm

# Create your views here.
# UTILIZANDO ORM DE DJANGO PARA CONSULTAS SQL
def inicio(req):
	personas = Persona.objects.all()			#select * from persona
	#print(personas)
	contexto = {
		'personas':personas
	}
	return render(req, 'index.html',contexto)

def crearPersona(req):
	if req.method == 'GET':
		form = PersonaForm()
		contexto = {
			'form': form
		}
	else:
		form =PersonaForm(req.POST)
		contexto = {
			'form': form
		}
		print(form)
		if form.is_valid():
			form.save()
			return redirect('index')
	return render(req, 'crear_persona.html',contexto)

def editarPersona(req,id):
	persona = Persona.objects.get(id = id)
	if req.method == 'GET':
		form = PersonaForm(instance = persona)
		contexto = {
			'form': form
		}
	else:
		form = PersonaForm(req.POST, instance = persona)
		contexto = {
			'form': form
		}
		if form.is_valid():
			form.save()
			return redirect('index')
	return render(req,'crear_persona.html',contexto)

def eliminarPersona(req,id):
	persona = Persona.objects.get(id = id)
	persona.delete()
	return redirect('index')
