from django.shortcuts import render,redirect
from django.views.generic import CreateView,DeleteView,ListView,UpdateView
from django.urls import reverse_lazy
from .forms import PersonaForm
from .models import Persona

"""
vistas basadas en clases
class View():
	model = Persona
	template_name = 'index.html'

	def dispatch()

	def get_context_data(self):
		context ={}
		context['queryset'] = self.get_queryset()
		return context

	dispatch : vericar el metodo de la solicitud http
	http_not_allowed

	def get_queryset(self):
		return self.model.objects.all()
	
	def get_templates_names():
		return self.template_name

	def post(self,req,*args,**kwargs):
		return render(request,self.get_templates_names(),self.get_queryset())
"""


class PersonaList(ListView):

	model = Persona
	template_name = 'index.html'

class PersonaCreate(CreateView):
	model = Persona
	form_class = PersonaForm
	template_name = 'crear_persona.html'
	success_url = reverse_lazy('index')

class PersonaUpdate(UpdateView):
	model = Persona
	form_class = PersonaForm
	template_name = 'crear_persona.html'
	success_url = reverse_lazy('index')

class PersonaDelete(DeleteView):
	model = Persona
	template_name = 'verificacion.html'
	success_url = reverse_lazy('index')
